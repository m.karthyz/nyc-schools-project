package com.example.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.model.SchoolService
import com.example.model.Schools
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SchoolsViewModel : ViewModel() {

    lateinit var schoolsListData : MutableLiveData<List<Schools>>

    init{
        schoolsListData = MutableLiveData()
    }

    fun getSchoolsDataObserver() : MutableLiveData<List<Schools>>{
        return schoolsListData
    }

    fun makeApiCall() {
        val schooolService = SchoolService.instance

        CoroutineScope(Dispatchers.IO).launch {
            var response  = schooolService.getSchoolDetails()
            if (response.isNotEmpty()) {
                schoolsListData.postValue(response as ArrayList<Schools>)
            }
        }
    }
}