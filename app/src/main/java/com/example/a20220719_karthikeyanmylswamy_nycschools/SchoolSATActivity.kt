package com.example.a20220719_karthikeyanmylswamy_nycschools

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.ui.AppBarConfiguration
import com.example.a20220719_karthikeyanmylswamy_nycschools.databinding.ActivitySchoolSatBinding
import com.example.model.SchoolDetails
import com.example.model.SchoolService
import com.example.viewmodel.SchoolsViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SchoolSATActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivitySchoolSatBinding
    private lateinit var satDetails: ArrayList<SchoolDetails>
    private var schoolDbn = ""
    private var schoolsViewModel = SchoolsViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //view binding
        binding = ActivitySchoolSatBinding.inflate(layoutInflater)
        setContentView(binding.root)
        schoolDbn = intent.getStringExtra("SCHOOL DBN").toString()
        makeApiCall()
    }

    private fun makeApiCall() {
        CoroutineScope(Dispatchers.IO).launch {
            val schooolService = SchoolService.instance
            satDetails = schooolService.getSATDetails() as ArrayList<SchoolDetails>
            withContext(Main){
                for(details in satDetails){
                    //compares the dbn from both the results and sets the details accordingly.
                    if(details.schoolDbn.equals(schoolDbn)){
                        binding.satMathScore.text = "SAT Math Score: " + details.satMathAverageScore  //no data displays a default message
                        binding.satReadingScore.text = "SAT Reading Score: " + details.satCriticalReadingScore
                        binding.satWritingScore.text = "SAT Writing Score: " + details.satWritingAverageScore
                        binding.schoolDetails.text = details.schoolName
                    }
                }
            }
        }
    }
}