package com.example.a20220719_karthikeyanmylswamy_nycschools

import android.content.Intent
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.adapter.SchoolsListAdapter
import com.example.model.SchoolService
import com.example.model.Schools
import com.example.viewmodel.SchoolsViewModel
import retrofit2.Retrofit

class MainActivity :  SchoolsListAdapter.SchoolDetailsClickListener, AppCompatActivity() {

    private var schoolsListAdapter : SchoolsListAdapter = SchoolsListAdapter()
    private var schoolsViewModel: SchoolsViewModel = SchoolsViewModel()
    private var dataForTheList = ArrayList<Schools>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initRecyclerView()
        observeLiveData()
        schoolsViewModel.makeApiCall()
        }

    private fun initRecyclerView() {
        findViewById<RecyclerView>(R.id.schoolsRecycleView).apply{
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = schoolsListAdapter
        }
    }

    //Observe live data during change to set data for the adapter.
    private fun observeLiveData(){
        schoolsViewModel.schoolsListData.observe(this){ result ->
            dataForTheList.clear()
            dataForTheList.addAll(result)
            schoolsListAdapter.setDataForAdapter(dataForTheList, this)
            schoolsListAdapter.notifyDataSetChanged()
        }
    }

    //implementation for get school details click.
    override fun onSchoolDetailsClick(schooldbn: String) {
        val intent = Intent(this, SchoolSATActivity::class.java)
        intent.putExtra("SCHOOL DBN", schooldbn)
        startActivity(intent)
    }
}
