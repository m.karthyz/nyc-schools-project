package com.example.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.a20220719_karthikeyanmylswamy_nycschools.R
import com.example.model.Schools
import java.time.Duration

class SchoolsListAdapter : RecyclerView.Adapter<SchoolsListAdapter.CustomViewHolder>() {

    var dataList: ArrayList<Schools> = ArrayList()
    lateinit var clickListener: SchoolDetailsClickListener
    class CustomViewHolder(view: View) : RecyclerView.ViewHolder(view){
        var schoolNameTextView: TextView = view.findViewById(R.id.schoolName)
        var schoolEmail: TextView = view.findViewById(R.id.schoolEmail)
        var getSchoolDetailsButton: TextView = view.findViewById(R.id.buttonGetSchoolDetails)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.recycle_item_row, parent, false)
        return CustomViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val item = dataList.get(position)
        holder.schoolNameTextView.text = "School Name: " + item.schoolName
        holder.schoolEmail.text = if(item.schoolEmailId!=null) "Email Id: " + item.schoolEmailId else "Email Id: "
        holder.getSchoolDetailsButton.setOnClickListener(View.OnClickListener {
            clickListener.onSchoolDetailsClick(item.schoolDbn)
        })
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    public fun setDataForAdapter(schoolsList : ArrayList<Schools>, listener : SchoolDetailsClickListener){
        dataList = schoolsList
        clickListener = listener
    }


    public interface SchoolDetailsClickListener{
        fun onSchoolDetailsClick(schooldbn : String)
    }
}