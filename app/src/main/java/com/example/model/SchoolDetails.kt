package com.example.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass


data class SchoolDetails(
    @field:Json(name = "dbn")val schoolDbn: String,
    @field:Json(name = "school_name")val schoolName: String,
    @field:Json(name = "sat_critical_reading_avg_score")val satCriticalReadingScore: String,
    @field:Json(name = "sat_math_avg_score")val satMathAverageScore: String,
    @field:Json(name = "sat_writing_avg_score")val satWritingAverageScore: String,
)

