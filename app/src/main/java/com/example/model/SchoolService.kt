package com.example.model

import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET

interface SchoolService {

    @GET("/resource/s3k6-pzi2.json")
    suspend fun getSchoolDetails() : List<Schools>

    @GET("resource/f9bf-2cp4.json")
    suspend fun getSATDetails(): List<SchoolDetails>

    companion object{
        private val retrofit by lazy{
            Retrofit.Builder()
                .baseUrl("https://data.cityofnewyork.us")
                .addConverterFactory(MoshiConverterFactory.create())
                .build()
        }

        val instance: SchoolService by lazy { retrofit.create(SchoolService::class.java) }
    }
}