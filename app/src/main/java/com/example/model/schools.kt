package com.example.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

data class Schools(
    @field:Json(name = "school_name")val schoolName: String,
    @field:Json(name = "school_email")val schoolEmailId: String,
    @field:Json(name = "dbn")val schoolDbn: String)



